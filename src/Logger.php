<?php

namespace Doctoreto\Log;

use Exception;
use Throwable;
use Bugsnag\Stacktrace;
use Psr\Log\AbstractLogger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Bugsnag\Configuration as BugsnagConfiguration;
use Doctoreto\Log\Models\Error;

class Logger extends AbstractLogger
{
    /**
     * Create a new doctoreto logger instance.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    /**
     * Log a message to the logs.
     *
     * @param string $level
     * @param mixed  $message
     * @param array  $context
     *
     * @return void
     */
    public function log($level, $message, array $context = [])
    {
        $exception = null;
        if (isset($context['exception']) && ($context['exception'] instanceof Exception || $context['exception'] instanceof Throwable)) {
            $exception = $context['exception'];
            unset($context['exception']);
        } elseif ($message instanceof Exception || $message instanceof Throwable) {
            $exception = $message;
        }

        // use bugsnag stacktrace class to generate codes frames from exception stacktrace
        $stacktrace = Stacktrace::fromBacktrace(
            new BugsnagConfiguration(''),
            $exception->getTrace(),
            $exception->getFile(),
            $exception->getLine()
        );

        $this->logToDatabase($level, $message, $stacktrace->toArray());
    }


    public function logToDatabase($level, $message, array $stacktrace = [])
    {
        $error = new Error();

        $error->level = $level;
        $error->user_uid = Auth::id();
        $error->message = $message;
        $error->request = $this->getRequestData();
        $error->stacktrace = $stacktrace;

        return $error->save();
    }
    /**
         * Get the request formatted as request data.
         *
         * @return array
         */
    private function getRequestData()
    {
        $data = [];

        $data['url'] = $this->request->fullUrl();

        $data['httpMethod'] = $this->request->getMethod();

        $data['params'] = $this->request->input();

        $data['clientIp'] = $this->request->getClientIp();

        if ($agent = $this->request->header('User-Agent')) {
            $data['userAgent'] = $agent;
        }

        if ($headers = $this->request->headers->all()) {
            $data['headers'] = $headers;
        }

        return $data;
    }
}
