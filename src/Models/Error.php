<?php

namespace Doctoreto\Log\Models;

use Illuminate\Database\Eloquent\Model;

class Error extends Model
{
    protected $guarded = [];

    protected $casts = [
        'request' => 'array',
        'stacktrace' => 'array',
    ];
}
